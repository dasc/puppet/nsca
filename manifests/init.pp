class nsca (
    String $client_package_name,
    String $service_package_name,
    String $client_config_file,
    String $service_config_file,
    String $password,
    Integer $encryption_method,
    Integer $port,
    String $user,
    String $group,
    Integer $max_packet_age,
    String $command_file,
    String $alternate_dump_file,
    Boolean $debug,
    Boolean $aggregate_writes,
    Boolean $client_enable,
    Boolean $service_enable,
) {
    if $client_enable {
	package {"${client_package_name}": ensure => installed, }

	file { "${client_config_file}": 
	    ensure => file,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('nsca/send_nsca.cfg.epp', {
		'password' => $password,
		'encryption_method' => $encryption_method,
	    }),
	    require => Package["${client_package_name}"],
	}
    }# else {
#	package {"${client_package_name}": ensure => absent, }
#
#	file { "${client_config_file}": 
#	    ensure => absent,
#	}
#    }

    if $service_enable {
	package {"${service_package_name}": ensure => installed, }

	file { "${service_config_file}":
	    ensure => file,
	    owner => 'root',
	    group => 'root',
	    mode => '0600',
	    content => epp('nsca/nsca.cfg.epp', {
		'password' => $password,
		'decryption_method' => $encryption_method,
		'max_packet_age' => $max_packet_age,
		'port' => $port,
		'user' => $user,
		'group' => $group,
		'command_file' => $command_file,
		'alternate_dump_file' => $alternate_dump_file,
		'debug' => $debug,
		'aggregate_writes' => $aggregate_writes,
	    }),
	    require => Package["${service_package_name}"],
	}

	service { 'nsca':
	    ensure => 'running',
	    enable => true,
	    subscribe => File["${service_config_file}"],
	    require => Package["${service_package_name}"],
	}
    }# else {
#	package {"${service_package_name}" : ensure => absent, }
#	file { "${service_config_file}" :
#	    ensure => absent,
#	}
 #   }
}
